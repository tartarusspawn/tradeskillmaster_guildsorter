## Author: j311yf1sh, tartarusspawn
## Interface: 60000
## Notes: Add's an operation that can be applied to groups in order to organise guild bank.
## Title: |cff00ff00TradeSkillMaster_GuildSorter|r
## Version: 2.0
## Dependency: TradeSkillMaster
## SavedVariables: TradeSkillMaster_GuildSorterDB
## SavedVariablesPerCharacter: gnametab
Locale\enUS.lua
TradeSkillMaster_GuildSorter.lua
Modules\Options.lua
Modules\Util.lua
Modules\Move.lua
Modules\Sort.lua
Modules\BankUI.lua

