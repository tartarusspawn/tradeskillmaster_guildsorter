local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter")
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

local TSM = select(2, ...)
local Sort = TSM:NewModule("Sort")
local Util = TSM:GetModule("Util")
local Move = TSM:GetModule("Move")
local Tab = {}
local current = nil -- Hold current item
local private = {}

Moves = {} -- Bags To Bank etc
moves = {} -- Guild Tab Sort

TSM_AtBankStatus = false
TSM.SortBank = false

local function getBagSlots()
	local t = {}
	for i = 1, NUM_BAG_SLOTS + 1 do
		t[i] = i - 1
	end
	return t
end

-- ============================================================================
-- Sort Guild Current Bank Tab Functions
-- ============================================================================
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

function StartGuildBankTabSort()

	if TSM_AtBankStatus then
		if TSM.PlayerGuild == nil then
			TSM.PlayerGuild = select(1,GetGuildInfo("player"))
		end
		local tab = GetCurrentGuildBankTab()
		local Tab = Sort:CreateTabItems(tab)
		Sort:SortTab( Tab,GSort_Tabs["tab"..tab]["ReverseSort"] )
		TSM:GDHchatSpam("Starting")
		TSMAPI.Delay:AfterTime( "RunSort",0.75,Move.RunSortTab,1 )
	else
		TSM:GDHchatSpam("Must be at guildbank")
	end
end

function Sort:CreateTabItems(tab)
	local items = 98
	local bag = {}
	for i=1, items, 1 do
		local item = {}
		local _, count = GetGuildBankItemInfo(tab, i)
		local link = GetGuildBankItemLink(tab, i)
		item.tab = tab
		item.slot = i
		item.name = "<EMPTY>"
		item.id = TSMAPI.Item:ToItemID(link)
		tempgsgrp = TSMAPI.Item:ToBaseItemString(item.id, true)
		item.gsgrp = TSMAPI.Groups:GetPath(tempgsgrp,true)
		if item.id ~= nil then
		    item.count = count
			item.name, _, item.quality, item.level, item.itemMinLevel, item.class, item.subclass, _,item.EquipLoc, _, item.price = GetItemInfo(link)
			if item.id == 82800 then
				item.speciesID, item.level, item.breedQuality, item.maxHealth, item.power, item.speed, item.name  = GameTooltip:SetGuildBankItem(tab,i)
				item.quality = item.breedQuality
				testsetting ="p:"..item.speciesID..":"..item.level..":"..item.breedQuality..":"..item.maxHealth..":"..item.power..":"..item.speed
				if (TSMAPI.Groups:GetPath( testsetting,true) == nil) then
					testsetting = "p:"..item.speciesID
				end
				item.gsgrp = TSMAPI.Groups:GetPath( testsetting,true)
			end
			if item.subclass == "Companion Pets" then
				item.class = "Battle Pets"
				item.subclass =" BattlePet"
			end
			if item.gsgrp == nil then 
				item.gsgrp = " "
				item.grpsort = 2
			else 
				item.grpsort = 1
			end
		end
		table.insert(bag, item)
	end		
	return bag;
end

function Sort:SortTab(Tab,ReverseSort)
	if ReverseSort == true then
		for i=#Tab,1,-1 do
			local lowest = i
			for j = 1, i - 1, 1 do
				if (Util:CompareItems(Tab[lowest],Tab[j]) == false) then
					lowest = j
				end
			end
			if i ~= lowest then
				local move = Util:CreateMove(Tab[lowest], Tab[i]);
				table.insert(moves, move);
				local tmp = Tab[i];
				Tab[i] = Tab[lowest];
				Tab[lowest] = tmp;
				tmp = Tab[i].slot;
				Tab[i].slot = Tab[lowest].slot;
				Tab[lowest].slot = tmp;
				tmp = Tab[i].bag;
				Tab[i].bag = Tab[lowest].bag;
				Tab[lowest].bag = tmp;
				tmp = Tab[i].tab;
				Tab[i].tab = Tab[lowest].tab;
				Tab[lowest].tab = tmp;
			end
		end
	else
		for i=1,#Tab,1 do
			local lowest = i
			for j = #Tab, i + 1, -1 do
				if (Util:CompareItems(Tab[lowest],Tab[j]) == false) then
					lowest = j
				end
			end
			if i ~= lowest then
				local move = Util:CreateMove(Tab[lowest], Tab[i]);
				table.insert(moves, move);
				local tmp = Tab[i];
				Tab[i] = Tab[lowest];
				Tab[lowest] = tmp;
				tmp = Tab[i].slot;
				Tab[i].slot = Tab[lowest].slot;
				Tab[lowest].slot = tmp;
				tmp = Tab[i].bag;
				Tab[i].bag = Tab[lowest].bag;
				Tab[lowest].bag = tmp;
				tmp = Tab[i].tab;
				Tab[i].tab = Tab[lowest].tab;
				Tab[lowest].tab = tmp;
			end
		end
	end
end

-- ============================================================================
-- Sort to guild bank functions
-- ============================================================================
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

function StartGbankPut(grptreeinfo)
	if TSM_AtBankStatus   then
		Util.get_playerGuild()
		SortBag_GenerateMoves(grptreeinfo)
		TSMAPI.Delay:AfterTime("RunSort",0.5,Move.RunSort,1)
	else

			TSM:GDHchatSpam("Must be at guildbank")

	end
end


function SortBag_GenerateMoves(grptreeinfo)
	wipe(Moves)
	TSM:GDHchatSpam("Depositing Items to Guildbank")
	for i, bag in ipairs(getBagSlots()) do
		for slot = 1, GetContainerNumSlots(bag) do
			local item =  TSMAPI.Item:ToBaseItemString( GetContainerItemID(bag,slot),true )
			if (item) then
				if item == "i:82800" then
					TSM:SysMessage(GetContainerItemLink( bag, slot ))
					item  = GetContainerItemLink( bag, slot  )
					if ( TSMAPI.Groups:GetPath( TSMAPI.Item:ToItemString( item ) ) == nil ) then
						if ( TSMAPI.Groups:GetPath( TSMAPI.Item:ToBaseItemString( item ) ) ~= nil ) then
							item = TSMAPI.Item:ToBaseItemString( item )
						else
							item = TSMAPI.Item:ToItemString( item )
						end
					else 
						item = TSMAPI.Item:ToItemString( item )
					end
					TSM:SysMessage(item)
				end
				local GBank, GBankTab , GBankBackupTab = Util:CheckTabAndGuild(item,grptreeinfo)
				if GBankTab == nil or GBank == nil then
					-- nothing
				else
					if TSM.PlayerGuild == GBank then 
						TSM:SysMessage(GBank.." equal "..TSM.PlayerGuild)
						tinsert(Moves,{id = item, Src = "bags", DestTab = GBankTab, DestBTab = GBankBackupTab, SrcBag = bag, SrcSlot = slot})
					else
						TSM:SysMessage(GBank .. " not equal to " .. TSM.PlayerGuild)
					end
				end
			end
		end
	end
end

-- ============================================================================
-- sort Gbank functions
-- ============================================================================
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

function StartGbankSort(grptreeinfo)
	local Gbankcache = true
		for ctab = 1, GetNumGuildBankTabs(),1 do


	end
	--if TSM_AtBankStatus  and (Gbankcache == true) then
	if TSM_AtBankStatus then
		Util.get_playerGuild()
		TSM.SortBank = true
		Sort.Bank_GenerateMoves(grptreeinfo)
		TSMAPI.Delay:AfterTime("RunSort",.5,Move.RunSortBank,1)
	else
		TSM:GDHchatSpam("Must be at guildbank")
	end
end



function Sort.Bank_GenerateMoves(grptreeinfo)
	wipe(Moves)
	TSM:GDHchatSpam("Sorting Guildbank")
	--SetCurrentGuildBankTab(cTab)
	for cTab = 1,GetNumGuildBankTabs(),1 do
		-- SetCurrentGuildBankTabSlot(slot)
		for slot = 1, 98,1 do
			local item = GetGuildBankItemLink(cTab,slot)
			local link = GetGuildBankItemLink(cTab,slot)
			if ( link ~= nil ) then
				item  = TSMAPI.Item:ToItemID(link)
				TSM:SysMessage(item)
				if item == 82800 then
					item = Util:GetBattlePetID("GuildBank", cTab, slot)
				end
				item = TSMAPI.Item:ToItemString(item)
				TSM:SysMessage(item)
				--if (TSMAPI.Inventory:GetGuildQuantity(item) < GSort_GeneralSettings.maxGbank  ) and ( TSMAPI.Inventory:GetBagQuantity(item) > GSort_GeneralSettings.maxbag )  then
				local GBank, GBankTab , GBankBackupTab = Util:CheckTabAndGuild(item,grptreeinfo)
				if GBankTab == nil or GBank == nil then
					-- nothing
				elseif cTab == GBankTab or cTab == GBankBackupTab then
					-- nothing
				else
					if TSM.PlayerGuild == GBank then 
						TSM:SysMessage(GBank.." equal "..TSM.PlayerGuild)
						tinsert(Moves,{id = item, Src = "guildbank", SrcTab = cTab,  SrcSlot = slot, DestTab = GBankTab , DestBTab = GBankBackupTab})
					else
						TSM:SysMessage(GBank .. " not equal to " .. TSM.PlayerGuild)
					end
				end
			end
		end

	end
	

end















