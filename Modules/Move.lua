
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

local TSM = select(2, ...)
local Move = TSM:NewModule("Move")
local Util = TSM:GetModule("Util")


function Move.RunSortBank()
	if #Moves > 0 or current ~= nil then
		if current ~= nil then
			if ( GetCursorInfo() )then
				local tarTab = Util:CheckTabsForFreeSpace( current.DestTab, current.DestBTab )
				if (tarTab ~= false) then
					Util:CheckTabForFreeSlot(tarTab)
					SetCurrentGuildBankTab( tarTab )
					PickupGuildBankItem( tarTab, DestSlot )
					else
						Util:CheckTabForFreeSlot( current.SrcTab )
						if (TSM.db.global["Guilds"][name]["Tabs"]["tab"..current.SrcTab]["ReverseSort"] == true) then 
							if DestSlot < current.SrcSlot then
								PickupGuildBankItem( current.SrcTab, current.SrcSlot )
							else 
								PickupGuildBankItem( current.SrcTab, DestSlot )
							end
						else
							if DestSlot > current.SrcSlot then
								PickupGuildBankItem( current.SrcTab, current.SrcSlot )
							else 
								PickupGuildBankItem( current.SrcTab, DestSlot )
							end
						end
					end
				QueryGuildBankTab( current.SrcTab )
			else
				current = nil
			end	
		else
			current = table.remove(Moves, 1)
			SetCurrentGuildBankTab(current.SrcTab)
			PickupGuildBankItem( current.SrcTab, current.SrcSlot )
		end
	else
		TSM:GDHchatSpam("Finished")
		TSMAPI.Delay:Cancel("RunSort")
	end
end

----------------------------------------------------------------------------------
-- old code
-- --------------------------------------------------------------------------------


function Move.RunSortTab()
	if #moves > 0 then
		if current ~= nil then    
			if CursorHasItem() then
				local type, id = GetCursorInfo();
				if current ~= nil and current.id == id then
					if current.sourcebag ~= nil then
						PickupContainerItem(current.targetbag, current.targetslot);
						local link = select(7, GetContainerItemInfo(current.targetbag, current.targetslot));
						if current.id ~= GetIDFromLink(link) then
							return;
						end
						
					end
				else
					moves = {};
					current = nil;
					return;
				end
			else
				if current.sourcebag ~= nil then
					local link = select(7, GetContainerItemInfo(current.targetbag, current.targetslot));
					if current.id ~= GetIDFromLink(link) then
						return;
					end
					current = nil;
				end
			end
		else      
			if #moves > 0 then
				current = table.remove(moves, 1);
				if current.sourcebag ~= nil then
					PickupContainerItem(current.sourcebag, current.sourceslot);
					if CursorHasItem() == false then
						return;
					end 
					PickupContainerItem(current.targetbag, current.targetslot);
					local link = select(7, GetContainerItemInfo(current.targetbag, current.targetslot));
					if current.id == GetIDFromLink(link) then
						current = nil;
					else
						return;
						end
				else
					PickupGuildBankItem(current.sourcetab, current.sourceslot);    
					PickupGuildBankItem(current.targettab, current.targetslot);    
					if CursorHasItem() then
						PickupGuildBankItem(current.sourcetab, current.sourceslot);    
					end
					current = nil;
					return;
				end
			end
		end
	else
		TSM:GDHchatSpam("Finished")
		TSMAPI.Delay:Cancel("RunSort")
	end
end

function Move.RunSort()
	if #Moves > 0 then
		local i = next(Moves)
		local Src = Moves[i].Src
		if Src == "guildbank" then
			local DestTab = Moves[i].DestTab
			local SrcTab = Moves[i].SrcTab
			local SrcSlot = Moves[i].SrcSlot
			local DestBTab = Moves[i].DestBTab

			if SrcTab ~= nil and SrcSlot ~= nil and DestTab ~= nil then
				if (Util:CheckTabForFreeSpace( DestTab ) == true) then 
					Util:CheckTabForFreeSlot(DestTab)
					SetCurrentGuildBankTab( SrcTab )
					PickupGuildBankItem( SrcTab, SrcSlot )
					SetCurrentGuildBankTab( DestTab )
					PickupGuildBankItem( DestTab, DestSlot )
				else
					DestTab = DestBTab
					if (Util:CheckTabForFreeSpace( DestTab ) == true) then 
						Util:CheckTabForFreeSlot(DestTab)
						SetCurrentGuildBankTab( SrcTab )
						PickupGuildBankItem( SrcTab, SrcSlot )
						SetCurrentGuildBankTab( DestTab )
						PickupGuildBankItem( DestTab, DestSlot )
					end
				end
			end
			tremove(Moves, i)
		elseif Src == "bags" then
			local SrcBag = Moves[i].SrcBag
			local SrcSlot = Moves[i].SrcSlot
			local DestTab = Moves[i].DestTab
			local DestBTab = Moves[i].DestBTab
			TSM:SysMessage(format("Bag %d, Slot %d, Tab %d",SrcBag, SrcSlot, DestTab))
			if SrcBag ~= nil and SrcSlot ~= nil and DestTab ~= nil then
				if (Util:CheckTabForFreeSpace( DestTab ) == true) then
					SetCurrentGuildBankTab(DestTab)
					UseContainerItem(SrcBag, SrcSlot)
				else 
					DestTab = DestBTab
					if (Util:CheckTabForFreeSpace( DestTab ) == true) then 
						SetCurrentGuildBankTab(DestTab)
						UseContainerItem(SrcBag, SrcSlot)
					end
				end
			end
			tremove(Moves, i)
		end
	else
		TSMAPI.Delay:Cancel("RunSort")
		TSM:GDHchatSpam("Finished")
		TSM.SortBank = false
	end

end