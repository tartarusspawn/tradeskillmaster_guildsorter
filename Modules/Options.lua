
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn

local TSM = select(2, ...)
local Options = TSM:NewModule("Options", "AceEvent-3.0", "AceHook-3.0")
local AceGUI = LibStub("AceGUI-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter")
local private = {}
gnametab ={}
for i = 1, GetNumGuildBankTabs() do
		gnametab[i]=gnametab[i] or (select(1,GetGuildBankTabInfo(i))) 
	end
	
-- ============================================================================
-- Module Options
-- ============================================================================
function Options:Load(container)
	if TSM.PlayerGuild == nil then
		TSM.PlayerGuild = select(1,GetGuildInfo("player"))
	end

	TSM:RequestTSMguildinfo()
	local tg = AceGUI:Create("TSMTabGroup")
	tg:SetLayout("Fill")
	tg:SetFullHeight(true)
	tg:SetFullWidth(true)
	tg:SetTabs({
		{value=1, text=L["General"]},	
		{value=2, text=gnametab[1] or "Tab 1" },
		{value=3, text=gnametab[2] or "Tab 2"},
		{value=4, text=gnametab[3] or "Tab 3"},
		{value=5, text=gnametab[4] or "Tab 4"},
		{value=6, text=gnametab[5] or "Tab 5"},
		{value=7, text=gnametab[6] or "Tab 6"},
		{value=8, text=gnametab[7] or "Tab 7"},
		{value=9, text=gnametab[8] or "Tab 8"},
	})
	tg:SetCallback("OnGroupSelected", function(self, _, value)
		self:ReleaseChildren()
		if value == 1 then
			private:DrawGeneral(self)
		else 
			private:DrawTab(self,(value - 1))

		end
	end)
	container:AddChild(tg)
	tg:SelectTab(1)
end

function private:DrawGeneral(container)
	local page = {
		{
			type = "ScrollFrame",
			layout = "List",
			children = {
				{
					type = "InlineGroup",
					layout = "flow",
					title = L["General Settings"],
					relativeWidth = 1,
					children = {
						{
							type = "CheckBox",
							label = L["Sort TSM Groups First"],
							settingInfo = { GSort_GeneralSettings, "groupfirst" },
							tooltip = L["Reserved for future use"],
							callback = function()
								TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["groupfirst"] = GSort_GeneralSettings.groupfirst
							end,
						},
					},
				},
				{
					type = "Spacer"
				},
				{
					type = "InlineGroup",
					layout = "flow",
					title = L["Ungrouped Items Settings"],
					relativeWidth = 1,
					children = {
						{
							type = "CheckBox",
							label = L["Sort Ungrouped Items"],
							settingInfo = { GSort_GeneralSettings, "SortUngrouped" },
							tooltip = L["Reserved for future use"],
							callback = function()
								TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["SortUngrouped"] 	= GSort_GeneralSettings.SortUngrouped	
							end,

						},
						{
							type = "Spacer"
						},
						{
							type = "Dropdown",
							label = "Tab:",
							settingInfo = {GSort_GeneralSettings, "GBankTab"},
							list = {[1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 6, [7] = 7, [8] = 8},
							relativeWidth = 0.5,
							multiselect = false,
							disabled = false,
							callback = function()
								TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankTab"] 		= GSort_GeneralSettings.GBankTab		
							end,
						},
						{
							type = "Dropdown",
							label = "Backup Tab:",
							settingInfo = {GSort_GeneralSettings, "GBankBackupTab"},
							list = {[1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 6, [7] = 7, [8] = 8},
							relativeWidth = 0.5,
							multiselect = false,
							disabled = false,
							callback = function()
								TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankBackupTab"]	= GSort_GeneralSettings.GBankBackupTab 			
							end,
						},
						--[[{
						type = "Slider",
						label = L["Max Guild Bank Stock"],
						settingInfo = {GSort_GeneralSettings,"maxGbank"},
						isPercent = false,
						min = 0,
						max = 200 * 98 * 8,
						step = 1,
						relativeWidth = 0.5,
						tooltip = L["Reserved for future use"],
						},
						{
						type = "Slider",
						label = L["Max Guild Bank Stock"],
						settingInfo = {GSort_GeneralSettings,"maxbag",},
						min = 0,
						max = 200 * 98 * 8,
						step = 1,
						relativeWidth = 0.5,
						tooltip = L["Reserved for future use"],
						},]]
					},
				},
			},
		},
	}
	--function() GSort_GeneralSettings.groupfirst = GetValue() end
	
	
	TSMAPI.GUI:BuildOptions(container, page)
end

function private:DrawTab(container,tab)

	local page = {
		{
			type = "ScrollFrame",
			layout = "list",
			children = {
				{
					type = "InlineGroup",
					layout = "flow",
					title = TSM.PlayerGuild,
					children = {
						{
							type = "CheckBox",
							label = L["Sort Tab From Right to Left"],
							settingInfo = { GSort_Tabs["tab"..tab], "ReverseSort"  },
							tooltip = L["Reserved for future use"],
							callback = function()
								TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab" .. tab]["ReverseSort"] = GSort_Tabs["tab"..tab]["ReverseSort"]
							end,
						},
					},
				},
			},
		},
	}

	TSMAPI.GUI:BuildOptions(container, page)
end
 


-- ============================================================================
-- Tooltip Options
-- ============================================================================

-- ============================================================================
-- Operation Options
-- ============================================================================



function Options:GetOperationOptionsInfo()
	TSM:RequestTSMguildinfo()
	local description = L["Guild Sorter operations allow for quick and easy sorting of items in guild banks."]
	
	local tabInfo = {
		{text = L["General"], callback = private.DrawOperationGeneral},
	}

	local relationshipInfo = {
		{ label = L["General Settings"] },
	}
	return description, tabInfo, relationshipInfo
end


function private.DrawOperationGeneral(container, operationName)
	local operationSettings = TSM.operations[operationName]

	local GuildList = {}
	GuildList["<None>"] = "<None>"
	for Name in pairs(TSM.db.global["Guilds"]) do
		GuildList[Name] = Name
	end
	
	local GuildBtabs = {}

	local page = {
		{
			-- scroll frame to contain everything
			type = "ScrollFrame",
			layout = "List",
			children = {
				{
					type = "InlineGroup",
					layout = "flow",
					title = "Operation Settings",
					children = {
						{
							type = "Dropdown",
							label = "Guild:",
							settingInfo = {operationSettings, "GBank"},
							list = GuildList,
							relativeWidth = 0.5,
							multiselect = false,
							disabled = false,
						},
						{
							type = "Dropdown",
							label = "Tab:",
							settingInfo = {operationSettings, "GBankTab"},
							list = {[1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 6, [7] = 7, [8] = 8},
							relativeWidth = 0.5,
							multiselect = false,
							disabled = false,
						},
						{
							type = "Dropdown",
							label = "Backup Tab:",
							settingInfo = {operationSettings, "GBankBackupTab"},
							list = {[1] = 1, [2] = 2, [3] = 3, [4] = 4, [5] = 5, [6] = 6, [7] = 7, [8] = 8},
							relativeWidth = 0.5,
							multiselect = false,
							disabled = false,
						},
					},
				},
			},
		},
	}

	TSMAPI.GUI:BuildOptions(container, page)
end

