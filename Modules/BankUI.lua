local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter")
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn

local TSM = select(2, ...)
local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter") -- loads the localization table
local BankUI = TSM:NewModule("BankUI", "AceEvent-3.0")
local private = {frame=nil, currentBank=nil}

function BankUI:OnEnable()
	BankUI:RegisterEvent("GUILDBANKFRAME_OPENED", function() 
		TSM:RequestTSMguildinfo()
		TSM_AtBankStatus = true 

		for ctab = 1, GetNumGuildBankTabs(),1 do
			QueryGuildBankTab( ctab )
			gnametab[ctab]=(select(1,GetGuildBankTabInfo(ctab))) 
		end

	end
		)
	BankUI:RegisterEvent("BANKFRAME_OPENED", function()
		TSM:RequestTSMguildinfo()
		TSM_AtBankStatus = true  
	end
		)
	BankUI:RegisterEvent("GUILDBANKFRAME_CLOSED", function() 
		TSMAPI.Delay:Cancel("RunSort")
		TSM_AtBankStatus = false
	end)
	--BankUI:RegisterEvent("BANKFRAME_CLOSED", function() private.currentBank = nil end)
end

function BankUI:createTab(parent)
	if private.frame then return private.frame end
	
	local BFC = TSMAPI.GUI:GetBuildFrameConstants()
	local frameInfo = {
		type = "Frame",
		parent = parent,
		hidden = true,
		points = "ALL",
		children = {
			{
				type = "GroupTreeFrame",
				key = "groupTree",
				groupTreeInfo = {"GuildSorter", "GuildSorter_Bank"},
				points = {{"TOPLEFT"}, {"BOTTOMRIGHT", 0, 137}},
			},
			{
				type = "Frame",
				key = "buttonFrame",
				points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT", 0, 0}, {"BOTTOMRIGHT"}},
				children = {
					{
						type = "Button",
						key = "btnSortToGuild",
						text = L["Move Selected to Guild Bank"],
						textHeight = 16,
						size = {0, 20},
						points = {{"TOPLEFT", 5, -5}, {"TOPRIGHT", -5, -5}},
						scripts = {"OnClick"},
					},
					--[[
					{
					type = "Button",
					key = "btnSortToBag",
					text = L["Move Selected to Bags"],
					textHeight = 16,
					size = {0, 20},
					points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT", 0, -5}, {"TOPRIGHT", BFC.PREV, "BOTTOMRIGHT", 0, -5}},
					scripts = {"OnClick"},
					},
					]]
					{
						type = "HLine",
						size = {0, 2},
						points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT", -5, -5}, {"TOPRIGHT", BFC.PREV, "BOTTOMRIGHT", 5, -5}},
					},
					{
						type = "Button",
						key = "btnSortGuild",
						text = L["Sort Guild Bank"],
						textHeight = 16,
						size = {0, 20},
						points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT",5, -5}, {"TOPRIGHT", BFC.PREV, "BOTTOMRIGHT", -5, -5}},
						scripts = {"OnClick"},
					},


					{
						type = "Button",
						key = "btnSortCurrent",
						text = L["Sort Current Guild Bank Tab"],
						textHeight = 16,
						size = {0, 20},
						points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT", 0, -5}, {"TOPRIGHT", BFC.PREV, "BOTTOMRIGHT", 0, -5}},
						scripts = {"OnClick"},
					},
					--[[
					{
					type = "CheckBox",
					key = "chkSortUngrouped",
					label = L["Sort Ungrouped Items"],
					settingInfo = { GSort_GeneralSettings, "SortUngrouped" },
					tooltip = L["Reserved for future use"],
					textHeight = 16,
					size = {0, 20},
					points = {{"TOPLEFT", BFC.PREV, "BOTTOMLEFT", 0, -5}, {"TOPRIGHT", BFC.PREV, "BOTTOMRIGHT", 0, -5}},
					callback = function()
					TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["SortUngrouped"] 	= GSort_GeneralSettings.SortUngrouped
					end,
					},
					]]
				},
			},
		},
		handlers = {
			buttonFrame = {
				btnSortToGuild= {
					OnClick = function() StartGbankPut( private.frame.groupTree:GetSelectedGroupInfo() ) end,
				},
				btnSortToBag= {
					OnClick = function()  end,
				},
				btnSortGuild = {
					OnClick = function() StartGbankSort( private.frame.groupTree:GetSelectedGroupInfo() ) end,
				},
				btnSortCurrent = {
					OnClick = function() StartGuildBankTabSort() end,
				},
			},
		},
	}
	
	private.frame = TSMAPI.GUI:BuildFrame(frameInfo)

	local helpPlateInfo = {
		FramePos = { x = -5, y = 100 },
		FrameSize = { width = 280, height = 490 },
		{
			ButtonPos = { x = 115, y = -66 },
			HighLightBox = { x = 0, y = -75, width = 280, height = 27 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["These will toggle between the module specific tabs."],
		},
		{
			ButtonPos = { x = 115, y = -196 },
			HighLightBox = { x = 0, y = -103, width = 275, height = 243 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["Lists the groups with GuildSort operations. Left click to select/deselect the group, Right click to expand/collapse the group."],
		},
		{
			ButtonPos = { x = 52.5, y = -335 },
			HighLightBox = { x = 0, y = -347, width = 136, height = 23 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["This button will select all groups."],
		},
		{
			ButtonPos = { x = 182.5, y = -335 },
			HighLightBox = { x = 138, y = -347, width = 136, height = 23 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["This button will de-select all groups."],
		},
		{
			ButtonPos = { x = -10, y = -361 },
			HighLightBox = { x = 0, y = -371, width = 275, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["This button will sort items from bags into bank pertaining to item operation settings."],
		},
		{
			ButtonPos = { x = 240, y = -388 },
			HighLightBox = { x = 0, y = -398, width = 275, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["This button will sort your Guild bank based on item operation settings."],
		},
		{
			ButtonPos = { x = -10, y = -417 },
			HighLightBox = { x = 0, y = -427, width = 275, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["This button sort's the current tab into order"],
		},
		{
			ButtonPos = { x = 240, y = -443 },
			HighLightBox = { x = 0, y = -453, width = 275, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["Reserved for future use"],
		},
		{
			ButtonPos = { x = -10, y = -470 },
			HighLightBox = { x = 0, y = -480, width = 136, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["Reserved for future use"],
		},
		{
			ButtonPos = { x = 240, y = -470 },
			HighLightBox = { x = 138, y = -480, width = 136, height = 25 },
			ToolTipDir = "RIGHT",
			ToolTipText = L["Reserved for future use"],
		},
	}

	local mainHelpBtn = CreateFrame("Button", nil, private.frame, "MainHelpPlateButton")
	mainHelpBtn:SetPoint("TOPRIGHT", private.frame, 45, 70)
	mainHelpBtn:SetScript("OnClick", function() BankUI:ToggleHelpPlate(private.frame, helpPlateInfo, mainHelpBtn, true) end)
	mainHelpBtn:SetScript("OnHide", function() if HelpPlate_IsShowing(helpPlateInfo) then BankUI:ToggleHelpPlate(private.frame, helpPlateInfo, mainHelpBtn, false) end end)

	return private.frame
end

function BankUI:ToggleHelpPlate(frame, info, btn, isUser)
	if not HelpPlate_IsShowing(info) then
		HelpPlate:SetParent(frame)
		HelpPlate:SetFrameStrata("DIALOG")
		HelpPlate_Show(info, frame, btn, isUser)
	else
		HelpPlate:SetParent(UIParent)
		HelpPlate:SetFrameStrata("DIALOG")
		HelpPlate_Hide(isUser)
	end
end