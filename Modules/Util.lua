local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter")
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn


local TSM = select(2, ...)
local Util = TSM:NewModule("Util")



function Util.get_playerGuild()

	if TSM.PlayerGuild == nil then
		TSM.PlayerGuild = select(1,GetGuildInfo("player"))
	end
	
end

function Util:GetBattlePetID(src,bag,slot)
	if src == "GuildBank" then
					local speciesID, level, breedQuality, maxHealth, power, speed, name  = GameTooltip:SetGuildBankItem( bag, slot )
					item ="p:"..speciesID..":"..level..":"..breedQuality..":"..maxHealth..":"..power..":"..speed..":"..name
					if (TSMAPI.Groups:GetPath( item,true) == nil) then
						item ="p:"..speciesID
					end
					return item
	end


end



function Util:CheckTabAndGuild(Item,grptreeinfo)
	local GBank, GBankTab , GBankBackupTab
	local operationName = TSMAPI.Operations:GetFirstByItem( Item, "GuildSorter" )
	if not operationName or not TSM.operations[operationName] then
		if (TSMAPI.Item:IsSoulbound(Item) == true) then
			-- do nothing
		else
			if (GSort_GeneralSettings.SortUngrouped == true) then
			
				local GBank 			= GSort_GeneralSettings.GBank
				local GBankTab 			= GSort_GeneralSettings.GBankTab
				local GBankBackupTab 	= GSort_GeneralSettings.GBankBackupTab
				
				TSM:SysMessage( "GBank = " .. GBank .. "  GBankTab = " .. GBankTab .. "  GBankBackupTab = " .. GBankBackupTab )
				return GBank, GBankTab , GBankBackupTab
			end
		end
	else
		TSM:SysMessage( "operationName = " .. operationName  )
		local operationPath = TSMAPI.Groups:GetPath(Item)
		TSM:SysMessage( "operationPath = " .. operationPath  )
		for k in pairs(grptreeinfo) do
			TSM:SysMessage(k)
			if k ~= operationPath then 
				--do nothing 
			else
				if (TSMAPI.Item:IsSoulbound(Item) == true) then
					-- do nothing
				else
					TSM:SysMessage( "operationName = " .. operationName  )
					local GBank = TSM.operations[operationName].GBank
					local GBankTab = TSM.operations[operationName].GBankTab
					local GBankBackupTab = TSM.operations[operationName].GBankBackupTab
					TSM:SysMessage( "GBank = " .. GBank .. "  GBankTab = " .. GBankTab .. "  GBankBackupTab = " .. GBankBackupTab )
					return GBank, GBankTab , GBankBackupTab
				end
			end
		end
	end
end


function Util:CheckTabForFreeSpace(dTab)
	local Count = 0
	for Slot = 1, 98, 1 do
		if (GetGuildBankItemInfo(dTab, Slot) == nil ) then
			return true
		end
	end
		return false
	end

function Util:CheckTabsForFreeSpace(dTab , bTab)
	local tarTab
	for dSlot = 1, 98, 1 do
		if (GetGuildBankItemInfo(dTab, dSlot) == nil ) then
			tarTab = dTab
			return tarTab
		end
	end
	if (bTab ~= dTab) then
		for bSlot = 1, 98, 1 do
			if (GetGuildBankItemInfo(bTab, bSlot) == nil ) then
				tarTab = bTab
				return tarTab
			end
		end
	end
		tarTab = false
		return tarTab
	end
	
function Util:CheckTabForFreeSlot(DestTab)

	local Count = 0
	if (TSM.db.global["Guilds"][name]["Tabs"]["tab"..DestTab]["ReverseSort"] == true) then 
		for Slot = 98, 1, -1 do
			local itemcheck = GetGuildBankItemInfo( DestTab, Slot )
			if (itemcheck)  then
			else
				DestSlot = Slot
				return DestSlot
			end
		end
		
	else
		for Slot = 1, 98, 1 do
			local itemcheck = GetGuildBankItemInfo( DestTab, Slot )
			if (itemcheck)  then
			else
			DestSlot = Slot
			return DestSlot
			end
		end
	end
end





function Util:CompareItems(lItem, rItem) -- comapres Left Item and Right Item hence lItem, and rItem
    if rItem.id == nil then
        return true;
    elseif lItem.id == nil then
        return false;
		elseif lItem.grpsort  ~= rItem.grpsort  and GSort_GeneralSettings.groupfirst == true then
			return (lItem.grpsort < rItem.grpsort );


		elseif lItem.gsgrp  ~= rItem.gsgrp  then
			return (lItem.gsgrp < rItem.gsgrp );
		elseif lItem.EquipLoc ~= rItem.EquipLoc then
			return (lItem.EquipLoc < rItem.EquipLoc);
		elseif lItem.class ~= rItem.class then
			return (lItem.class < rItem.class);
		elseif lItem.subclass ~= rItem.subclass then
			return (lItem.subclass < rItem.subclass);
		elseif lItem.id ~= rItem.id then
			return (lItem.id < rItem.id);
		elseif lItem.itemMinLevel ~= rItem.itemMinLevel  then
			return (lItem.itemMinLevel < rItem.itemMinLevel);
		elseif lItem.quality ~= rItem.quality then
			return (lItem.quality > rItem.quality);
		elseif lItem.name ~= rItem.name then
			return (lItem.name < rItem.name);
		elseif lItem.count ~= rItem.count then
			return (lItem.count >= rItem.count);
		else
			return true;
		end
end

function Util:CreateMove(source, target)
    local move = {};
    if source.id ~= nil then
        move.id = source.id;
        move.name = source.name;
        move.sourcebag = source.bag;
        move.sourcetab = source.tab;
        move.sourceslot = source.slot;
        move.targetbag = target.bag;
        move.targettab = target.tab;
        move.targetslot = target.slot;
    else
        move.id = target.id;
        move.name = target.name;
        move.sourcebag = target.bag;
        move.sourcetab = target.tab;
        move.sourceslot = target.slot;
        move.targetbag = source.bag;
        move.targettab = source.tab;
        move.targetslot = source.slot;
    end
    return move;    
end



