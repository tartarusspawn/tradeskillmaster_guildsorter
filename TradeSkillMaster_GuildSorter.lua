TSMGSdebug = false

-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn
-- TSM:SysMessage(msg)
-- TSM:GDHchatSpam(msg)

local variablesLoaded = 0
local TSM = select(2, ...)
TSM = LibStub("AceAddon-3.0"):NewAddon(TSM, "TSM_GuildSorter", "AceEvent-3.0", "AceConsole-3.0")
local AceGUI = LibStub("AceGUI-3.0")
local L = LibStub("AceLocale-3.0"):GetLocale("TradeSkillMaster_GuildSorter")
TSM.version = GetAddOnMetadata("TradeSkillMaster_GuildSorter", "Version")
local private = {}
GuildInfo()

local operationDefaults = {
	GBank = "<None>",
	GBankTab = 1,
	GBankBackupTab = 1,
}

local savedDBDefaults = {
	global = {
		Guilds = {
			["*"] = {
				GuildName= "",
				GeneralSettings = {},
				Tabs = {
					tab1 = {},
					tab2 = {},
					tab3 = {},
					tab4 = {},
					tab5 = {},
					tab6 = {},
					tab7 = {},
					tab8 = {},
				},
			},
		},
	},
}

GSort_GeneralSettings = {
	groupfirst		= false,
	SortUngrouped	= false,
	GBank			= "",
	GBankTab		= 1,
	GBankBackupTab	= 1,
}

GSort_Tabs = {
	tab1 = {
		ReverseSort		= false,
		viewed			= false,
	},
	tab2 = {
		ReverseSort = false,
		viewed			= false,
	},
	tab3 = {
		ReverseSort = false, 
	},
	tab4 = {
		ReverseSort = false,
		viewed			= false,
	},
	tab5 = {
		ReverseSort = false,
		viewed			= false,
	},
	tab6 = {
		ReverseSort = false,
		viewed			= false,
	},
	tab7 = {
		ReverseSort = false,
		viewed			= false,
	},
	tab8 = {
		ReverseSort = false,
		viewed			= false,
	},
}
function TSM:OnInitialize()
	
end

function TSM:OnEnable()

	local GuildList = {}
	GuildList["<None>"] = "<None>"

	for moduleName, module in pairs(TSM.modules) do
		TSM[moduleName] = module
	end

	TSM.db = LibStub:GetLibrary("AceDB-3.0"):New("TradeSkillMaster_GuildSorterDB", savedDBDefaults, true)
	TSM:RegisterModule()
	for name in pairs(TSM.operations) do
		TSMAPI.Operations:Update("GuildSorter", moduleName)
	end
end

function TSM:OnDisable()
end

function TSM:SysMessage(msg)
	if(TSMGSdebug == true) then
		self:Print("SYS Msg: "..msg);
	end
end

function TSM:GDHchatSpam(msg)
	if(TSMGSchatSpam == true) then
		self:Print(msg);
	end
end

function TSM:RegisterModule()
	--[[TSM.slashCommands = {
	{ key = "SortToGuildBank", label = L["Will sort items from bags into bank pertaining to item operation settings."], callback = "Sort:StartGbankPut" },
	{ key = "SortGuildBank", label = L["Will sort your Guild bank based on item operation settings."], callback = "Sort:StartGbankSort" },
	{ key = "SortGuildBankTab", label = L["Sort's the current tab into order"], callback = "SortStartGuildBankTabSort(TSM.db.global.GBankSortingOrder)" },
	}]]
	TSM.bankUiButton = { callback = "BankUI:createTab" }
	TSM.moduleOptions = { callback = "Options:Load" }
	TSM.operations = { maxOperations=11, callbackOptions="Options:GetOperationOptionsInfo", callbackInfo="GetOperationInfo", defaults = operationDefaults }
	TSMAPI:NewModule(TSM)
end


function TSM:GetOperationInfo(operationName)
	TSMAPI.Operations:Update("GuildSorter", operationName)
	local operation = TSM.operations[operationName]
	if not operation then return end
	if operation.target == "" then return end
	return format("Group Sorting in guild %s for tab %d", operation.GBank, operation.GBankTab)
end

function TSM:RequestTSMguildinfo()
	if (variablesLoaded == 0) then
		name = TSMAPI.Player:GetPlayerGuild(UnitName("player")) or GetGuildInfo("player")
		private.build_variables(name)
		private.Load_Variables(name)
		variablesLoaded = 1
	end
end

function private.build_variables(name)
	TSM.db.char.chatspam  = TSM.db.char.chatspam  or true 
	TSM.db.global["Guilds"][name]["GuildName"] = TSM.db.global["Guilds"][name]["GuildName"]  or name
	-- GeneralSettings Variables
	TSM.db.global["Guilds"][name]["GeneralSettings"]["groupfirst"]		= TSM.db.global["Guilds"][name]["GeneralSettings"]["groupfirst"] or false
	TSM.db.global["Guilds"][name]["GeneralSettings"]["SortUngrouped"]	= TSM.db.global["Guilds"][name]["GeneralSettings"]["SortUngrouped"] or false
	TSM.db.global["Guilds"][name]["GeneralSettings"]["GBank"]			= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBank"] or name
	TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankTab"]		= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankTab"] or 1
	TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankBackupTab"]	= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankBackupTab"] or 1
	TSM.db.global["Guilds"][name]["GeneralSettings"]["maxGbank"]		= TSM.db.global["Guilds"][name]["GeneralSettings"]["maxGbank"] or 0
	TSM.db.global["Guilds"][name]["GeneralSettings"]["maxbag"]			= TSM.db.global["Guilds"][name]["GeneralSettings"]["maxbag"] or 0
	-- GBankTab Variables
	for i = 1, 8,1 do
		TSM.db.global["Guilds"][name]["Tabs"]["tab"..i]["ReverseSort"]	= TSM.db.global["Guilds"][name]["Tabs"]["tab"..i]["ReverseSort"] or false 
	end
end

function private.Load_Variables(name)
	TSM:SysMessage( "Guild Name = " .. name  )
	TSMGSchatSpam							= TSM.db.char.chatspam
	GSort_GeneralSettings.groupfirst		= TSM.db.global["Guilds"][name]["GeneralSettings"]["groupfirst"]
	GSort_GeneralSettings.SortUngrouped		= TSM.db.global["Guilds"][name]["GeneralSettings"]["SortUngrouped"]
	GSort_GeneralSettings.GBank 			= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBank"]
	GSort_GeneralSettings.GBankTab			= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankTab"]
	GSort_GeneralSettings.GBankBackupTab 	= TSM.db.global["Guilds"][name]["GeneralSettings"]["GBankBackupTab"]
	GSort_GeneralSettings.maxGbank  		= TSM.db.global["Guilds"][name]["GeneralSettings"]["maxGbank"]
	GSort_GeneralSettings.maxbag  			= TSM.db.global["Guilds"][name]["GeneralSettings"]["maxbag"]
	for i = 1, 8,1 do
		GSort_Tabs["tab"..i]["ReverseSort"] = TSM.db.global["Guilds"][name]["Tabs"]["tab"..i]["ReverseSort"]
	end
end
