-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn

local L = LibStub("AceLocale-3.0"):NewLocale("TradeSkillMaster_GuildSorter", "enUS", true)




L["General Settings"] = true
L["General"] = true 
L["Guild Bank Tab Sorting Order"] = true
L["Guild Sorter operations allow for quick and easy sorting of items in guild banks."] = true
L["Keep in Bags"] = true
L["Lists the groups with GuildSort operations. Left click to select/deselect the group, Right click to expand/collapse the group."] = true
L["Max Guild Bank Stock"] = true
L["Move Selected to Bags"] = true
L["Move Selected to Guild Bank"] = true
L["Normal Sorting Order"] = true
L["Organize Current Guild Bank Tab"] = true
L["Reserved for future use"] = true
L["Reverse Sorting Order"] = true
L["Sort Current Guild Bank Tab"] = true
L["Sort Current Guild Bank Tab"] = true
L["Sort Guild Bank"] = true
L["Sort Tab From Right to Left"] = true
L["Sort TSM Groups First"] = true
L["Sort Ungrouped Items"] = true
L["Sort's the current tab into order"] = true
L["Tab Sorting Order"] = true
L["Tab"] = true
L["These will toggle between the module specific tabs."] = true
L["This button sort's the current tab into order"] = true
L["This button will de-select all groups."] = true
L["This button will select all groups."] = true
L["This button will sort items from bags into bank pertaining to item operation settings."] = true
L["This button will sort your Guild bank based on item operation settings."] = true
L["Ungrouped Items Settings"] = true
L["Will sort items from bags into bank pertaining to item operation settings."] = true
L["Will sort your Guild bank based on item operation settings."] = true

