
-- --------------------------------------------------------------------------------
--                          TradeSkillMaster_GuildSorter                         --
--          http://www.curse.com/addons/wow/tradeskillmaster_guildsorter         --
--                                                                               --
-- --------------------------------------------------------------------------------
-- Author      : j311yf1sh, tartarusspawn

GSort_GeneralSettings = {
	groupfirst = false,
}

GSort_Tabs = {
	tab1 = {
		ReverseSort  = false,
	},
	tab2 = {
		ReverseSort = false, 
	},
	tab3 = {
		ReverseSort = false, 
	},
	tab4 = {
		ReverseSort = false,
	},
	tab5 = {
		ReverseSort = false,
	},
	tab6 = {
		ReverseSort = false,
	},
	tab7 = {
		ReverseSort = false,
	},
	tab8 = {
		ReverseSort = false,
	},
}



-- GSort_GeneralSettings.  = TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"][""]

GSort_GeneralSettings.groupfirst		= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["groupfirst"]
GSort_GeneralSettings.SortUngrouped		= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["SortUngrouped"]
GSort_GeneralSettings.GBank 			= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBank"]
GSort_GeneralSettings.GBankTab			= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankTab"]
GSort_GeneralSettings.GBankBackupTab 	= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankBackupTab"]
GSort_GeneralSettings.maxGbank  		= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["maxGbank"]		=
GSort_GeneralSettings.maxbag  			= TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["maxbag"]
	
--[[
GSort_Tabs.tab1.
GSort_Tabs.tab2.
GSort_Tabs.tab3.
GSort_Tabs.tab4.
GSort_Tabs.tab5.
GSort_Tabs.tab6.
GSort_Tabs.tab7.
GSort_Tabs.tab8.
]] 
GSort_Tabs.tab1.ReverseSort	= TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab1"]["ReverseSort"]
GSort_Tabs.tab2.ReverseSort	= TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab2"]["ReverseSort"]
GSort_Tabs.tab3.ReverseSort	= TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab3"]["ReverseSort"]
GSort_Tabs.tab4.ReverseSort = TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab4"]["ReverseSort"]
GSort_Tabs.tab5.ReverseSort = TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab5"]["ReverseSort"]
GSort_Tabs.tab6.ReverseSort = TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab6"]["ReverseSort"]
GSort_Tabs.tab7.ReverseSort = TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab7"]["ReverseSort"]
GSort_Tabs.tab8.ReverseSort = TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab8"]["ReverseSort"]





--Saved Variables
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["groupfirst"] 		= GSort_GeneralSettings.groupfirst		
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["SortUngrouped"] 	= GSort_GeneralSettings.SortUngrouped		
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBank"] 			= GSort_GeneralSettings.GBank 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankTab"] 		= GSort_GeneralSettings.GBankTab			
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["GBankBackupTab"]	= GSort_GeneralSettings.GBankBackupTab 	
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["maxGbank"]			= GSort_GeneralSettings.maxGbank  		
TSM.db.global["Guilds"][TSM.PlayerGuild]["GeneralSettings"]["maxbag"]         	= GSort_GeneralSettings.maxbag  			


TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab1"]["ReverseSort"] = GSort_Tabs.tab1.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab2"]["ReverseSort"] = GSort_Tabs.tab2.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab3"]["ReverseSort"] = GSort_Tabs.tab3.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab4"]["ReverseSort"] = GSort_Tabs.tab4.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab5"]["ReverseSort"] = GSort_Tabs.tab5.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab6"]["ReverseSort"] = GSort_Tabs.tab6.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab7"]["ReverseSort"] = GSort_Tabs.tab7.ReverseSort 			
TSM.db.global["Guilds"][TSM.PlayerGuild]["Tabs"]["tab8"]["ReverseSort"] = GSort_Tabs.tab8.ReverseSort






